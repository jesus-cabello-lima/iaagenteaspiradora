/* The general structure is to put the AI code in xyz.js and the visualization
   code in c_xyz.js. Create a diagram object that contains all the information
   needed to draw the diagram, including references to the environment&agents.
   Then use a draw function to update the visualization to match the data in
   the environment & agent objects. Use a separate function if possible for 
   controlling the visualization (whether through interaction or animation). 
   Chapter 2 has minimal AI and is mostly animations. */
const SIZE = 100;
const colors = {
    perceptBackground: 'hsl(240,10%,85%)',
    perceptHighlight: 'hsl(60,100%,90%)',
    actionBackground: 'hsl(0,0%,100%)',
    actionHighlight: 'hsl(150,50%,80%)'
};
/* Create a matrix diagram object that includes the world (model) and the svg
   elements (view) */
function makeMatrixDiagram(selector) {
    let diagramMatrix = {}, worldMatrix = new WorldMatrix(5,5);
    diagramMatrix.worldMatrix = worldMatrix;
    
    diagramMatrix.xPosition = (floorNumber) => 150 + floorNumber * (150 * (diagramMatrix.worldMatrix.numFilas + 1) ) / (diagramMatrix.worldMatrix.numFilas + 1);
    diagramMatrix.yPosition = (floorNumber) => 150 + floorNumber * (150 * (diagramMatrix.worldMatrix.numColumnas + 1) ) / (diagramMatrix.worldMatrix.numColumnas + 1); ;

    diagramMatrix.root = d3.select(selector);
    diagramMatrix.robot = diagramMatrix.root.append('g')
        .attr('class', 'robot')
        .style('transform', `translate(${diagramMatrix.xPosition(worldMatrix.locationX)}px, ${diagramMatrix.yPosition(worldMatrix.locationY)}px)`);
    diagramMatrix.robot.append('rect')
        .attr('width', SIZE)
        .attr('height', SIZE)
        .attr('fill', 'hsl(120,25%,50%)');
    diagramMatrix.perceptText = diagramMatrix.robot.append('text')
        .attr('x', SIZE/2)
        .attr('y', -25)
        .attr('text-anchor', 'middle');
    diagramMatrix.actionText = diagramMatrix.robot.append('text')
        .attr('x', SIZE/2)
        .attr('y', -10)
        .attr('text-anchor', 'middle');

    diagramMatrix.matrixRow = [];
    for (let floorNumberX = 0; floorNumberX <= diagramMatrix.worldMatrix.numFilas; floorNumberX++) {
        let matrixRow = [];
            
        for (let floorNumberY = 0; floorNumberY <= diagramMatrix.worldMatrix.numColumnas; floorNumberY++) {
            
            matrixRow[floorNumberY] =
                diagramMatrix.root.append('rect')
                .attr('class', 'clean floor') // for css
                .attr('x', diagramMatrix.xPosition(floorNumberX))
                .attr('y', diagramMatrix.yPosition(floorNumberY))
                .attr('width', SIZE)
                .attr('height', SIZE/4)
                .attr('stroke', 'black')
                .on('click', function() {
                    if(!diagramMatrix.worldMatrix.floorsRow[floorNumberX][floorNumberY].cancelled){
                        diagramMatrix.worldMatrix.markFloorDirtyMatrix(floorNumberX, floorNumberY);
                        matrixRow[floorNumberY].attr('class', 'dirty floor');
                    }else{
                        diagramMatrix.worldMatrix.markFloorDirtyMatrix(floorNumberX, floorNumberY);
                        matrixRow[floorNumberY].attr('class', 'cancelled floor');
                    }
                })
                .on('dblclick', function() {
                    diagramMatrix.worldMatrix.markFloorCancelledMatrix(floorNumberX, floorNumberY);
                    if(diagramMatrix.worldMatrix.floorsRow[floorNumberX][floorNumberY].cancelled){
                        console.log(diagramMatrix.worldMatrix.floorsRow[floorNumberX][floorNumberY]);
                        matrixRow[floorNumberY].attr('class', 'cancelled floor');
                    } else {
                        console.log(diagramMatrix.worldMatrix.floorsRow[floorNumberX][floorNumberY]);
                        diagramMatrix.worldMatrix.markFloorDirtyMatrix(floorNumberX, floorNumberY);
                        matrixRow[floorNumberY].attr('class', 'dirty floor');
                    }
                });
        }

        diagramMatrix.matrixRow[floorNumberX] = matrixRow;
    }
    return diagramMatrix;
}


/* Rendering functions read from the state of the world (diagram.world) 
   and write to the state of the diagram (diagram.*). For most diagrams
   we only need one render function. For the vacuum cleaner example, to
   support the different styles (reader driven, agent driven) and the
   animation (agent perceives world, then pauses, then agent acts) I've
   broken up the render function into several. */

function renderMatrixWorld(diagramMatrix) {
    for (let floorNumberX = 0; floorNumberX <= diagramMatrix.worldMatrix.numFilas; floorNumberX++) {            
        for (let floorNumberY = 0; floorNumberY <= diagramMatrix.worldMatrix.numColumnas; floorNumberY++) {
            if(diagramMatrix.worldMatrix.floorsRow[floorNumberX][floorNumberY].cancelled){
                diagramMatrix.matrixRow[floorNumberX][floorNumberY].attr('class','cancelled floor');    
            } else {
                diagramMatrix.matrixRow[floorNumberX][floorNumberY].attr('class', diagramMatrix.worldMatrix.floorsRow[floorNumberX][floorNumberY].dirty? 'dirty floor' : 'clean floor');
            }
        }
    }
    diagramMatrix.robot.style('transform', `translate(${diagramMatrix.xPosition(diagramMatrix.worldMatrix.locationX)}px,${diagramMatrix.yPosition(diagramMatrix.worldMatrix.locationY)}px)`);
}

function renderAgentPerceptMatrix(diagramMatrix, dirty) {
    let perceptLabel = {false: "It's clean", true: "It's dirty"}[dirty];
    diagramMatrix.perceptText.text(perceptLabel);
}

function renderAgentActionMatrix(diagramMatrix, action) {
    let actionLabel = {null: 'Waiting', 'SUCK': 'Vacuuming', 'LEFT': 'Going left', 'RIGHT': 'Going right','UP': 'Going Up', 'DOWN': 'Going down'}[action];
    diagramMatrix.actionText.text(actionLabel);
}

const STEP_TIME_MS = 2500;

function makeReaderControlledDiagramMatrix() {
    let diagramMatrix = makeMatrixDiagram('#reader-controlled-diagram-matrix svg');
    let nextAction = null;
    let animating = false; // either false or a setTimeout intervalID
    
    function makeButton(action, label, x) {
        let button = d3.select('#reader-controlled-diagram-matrix .buttons')
            .append('button')
            .attr('class', 'btn btn-default')
            .style('position', 'absolute')
            .style('left', x + 'px')
            .style('width', '100px')
            .text(label)
            .on('click', () => {
                setAction(action);
                updateButtons();
            });
        button.action = action;
        return button;
    }

    let buttons = [
        makeButton('LEFT', 'Move left', 150),
        makeButton('SUCK', 'Vacuum', 300),
        makeButton('RIGHT', 'Move right', 450),
        makeButton('UP', 'Move up', 600),
        makeButton('DOWN', 'Move down', 750),
    ];

    function updateButtons() {
        for (let button of buttons) {
            button.classed('btn-warning', button.action == nextAction);
        }
    }

    function setAction(action) {
        nextAction = action;
        if (!animating) { updateMatrix(); }
        
    }
    
    function updateMatrix() {
        let percept = diagramMatrix.worldMatrix.floorsRow[diagramMatrix.worldMatrix.locationX][diagramMatrix.worldMatrix.locationY].dirty;
        
        if (nextAction !== null) {
            diagramMatrix.worldMatrix.simulateMatrix(nextAction);
            renderMatrixWorld(diagramMatrix);
            renderAgentPerceptMatrix(diagramMatrix, percept);
            renderAgentActionMatrix(diagramMatrix, nextAction);
            nextAction = null;
            updateButtons();
            animating = setTimeout(updateMatrix, STEP_TIME_MS);
        } else {
            animating = false;
            renderMatrixWorld(diagramMatrix);
            renderAgentPerceptMatrix(diagramMatrix, percept);
            renderAgentActionMatrix(diagramMatrix, null);
        }
    }
}

function makeAgentControlledDiagramMatrix() {
    let diagramMatrix = makeMatrixDiagram('#agent-controlled-diagram-matrix svg');

    function update() {
        let percept = diagramMatrix.worldMatrix.floorsRow[diagramMatrix.worldMatrix.locationX][diagramMatrix.worldMatrix.locationY].dirty;
        let action = reflexVacuumAgentMatrix(diagramMatrix.worldMatrix);
        diagramMatrix.worldMatrix.simulateMatrix(action);
        
        renderMatrixWorld(diagramMatrix);
        renderAgentPerceptMatrix(diagramMatrix, percept);
        renderAgentActionMatrix(diagramMatrix, action);
    }
    update();
    setInterval(update, STEP_TIME_MS);
}

makeAgentControlledDiagramMatrix();
makeReaderControlledDiagramMatrix();