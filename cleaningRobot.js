// In this simple problem the world includes both the environment and the robot
// but in most problems the environment and world would be separate
class World {
    constructor(numFloors) {
        this.location = 0;
        this.numFloors = numFloors - 1;
        this.floors = [];
        this.lastAction = 'RIGHT';
        for (let i = 0; i < numFloors; i++) {
            this.floors.push({dirty: false});
        }
    }

    markFloorDirty(floorNumber) {
        this.floors[floorNumber].dirty = true;
    }

    simulate(action) {
        switch(action) {
        case 'SUCK':
            this.floors[this.location].dirty = false;
            break;
        case 'LEFT':
            if(this.location > 0)
                this.location -= 1;
            break;
        case 'RIGHT':
            if(this.location < this.numFloors)
                this.location += 1;
            break;
        }

        return action;
    }
}

class WorldMatrix {
    constructor(numFilas, numColumnas) {
        this.locationX = 0;
        this.locationY = 0;
        this.performance = 0;

        this.numFilas = numFilas - 1;
        this.numColumnas = numColumnas - 1;
        
        this.floorsRow = [];
        
        this.lastAction = 'RIGHT';
        
        for(let i = 0; i < numFilas; i++) {
            let floorsFila = [];
            for (let j = 0; j < numColumnas; j++) {
                floorsFila.push({dirty: false, cancelled: false});
            }

            this.floorsRow.push(floorsFila);
        }
    }

    markFloorDirtyMatrix(floorX, floorY) {
        this.floorsRow[floorX][floorY].dirty = true;
    }

    markFloorCancelledMatrix(floorX, floorY) {
        if(this.floorsRow[floorX][floorY].cancelled)
            this.floorsRow[floorX][floorY].cancelled = false;
        else
            this.floorsRow[floorX][floorY].cancelled = true;
    }

    simulateMatrix(action) {
        switch(action) {
        case 'SUCK':
            this.floorsRow[this.locationX][this.locationY].dirty = false;
            break;
        case 'LEFT':
            if(this.locationX > 0)
                if(!this.floorsRow[this.locationX-1][this.locationY].cancelled){
                    this.locationX -= 1;
                }else{
                    action = null;
                }
            break;
        case 'RIGHT':
            if(this.locationX < this.numFilas)
                if(!this.floorsRow[this.locationX + 1][this.locationY].cancelled){
                    this.locationX += 1;
                }else{
                    action = null;
                }
            break;
        case 'UP':
            if(this.locationY > 0)
                if(!this.floorsRow[this.locationX][this.locationY - 1].cancelled){
                    this.locationY -= 1;
                }else{
                    action = null;
                }
            break;
        case 'DOWN':
            if(this.locationY < this.numColumnas)
                if(!this.floorsRow[this.locationX][this.locationY + 1].cancelled){    
                    this.locationY += 1;
                }else{
                    action = null;
                }
            break;
        }
        this.performance += 1;
        console.log("Performance: "+this.performance); 
        return action;
    }
}

// Rules are defined in code
function reflexVacuumAgent(world) {
    if (world.floors[world.location].dirty)              { return 'SUCK'; }
    else if (world.location == 0)                        { world.lastAction = 'RIGHT'; return 'RIGHT'; }
    else if (world.location == world.numFloors)          { world.lastAction = 'LEFT'; return 'LEFT'; }
    else { return world.lastAction; }
}

// Rules are defined in code
function reflexVacuumAgentMatrix(world) {
    let actionLabel = ['LEFT', 'RIGHT','UP', 'DOWN'];

    let action = actionLabel[Math.floor(Math.random() * actionLabel.length)];

    if (world.floorsRow[world.locationX][world.locationY].dirty) { return 'SUCK'; }
    else { return action; }
}

// Rules are defined in data, in a table indexed by [location][dirty]
function tableVacuumAgent(world, table) {
    let location = world.location;
    let dirty = world.floors[location].dirty ? 1 : 0;
    return table[location][dirty];
}
